# BoB schema traveller-category-table

It is RECOMMENDED that the categories in [travellerCategoryTable.json](https://bitbucket.org/samtrafiken/bob-schema-traveller-category-table/src/master/travellerCategoryTable.json) are used for the _cat_ property in the data structure _c_md_ (Ticket Metadata) within the payloadData of the MTB (as specified in [MTS1](https://bitbucket.org/samtrafiken/bob-mts-1-mobile-ticket-format/src/master/mts1.pdf) and [MTS3](https://bitbucket.org/samtrafiken/bob-mts-3-coordinated-ticket-namespace/src/master/mts3.pdf)).

It is RECOMMENDED that the categories in [travellerCategoryTable.json](https://bitbucket.org/samtrafiken/bob-schema-traveller-category-table/src/master/travellerCategoryTable.json) are used in the [bob-api-product](https://bitbucket.org/samtrafiken/bob-api-product/src/master/product.yaml) schema for the _cat_ property in the _travellersPerCategory_ data structure and for the _travellerCategoryId_ property in the _travellerCategory_ data structure.

This is a part of the [BoB - National Ticket and Payment Standard](https://bob.samtrafiken.se), hosted by Samtrafiken i Sverige AB